#!/usr/bin/env sh

# script returns semantical versioning string linke 2.0.0 (if $1 is v2.0.0) or $1
if echo -n $1 | grep -Eq 'v[0-9]+\.[0-9]+\.[0-9]+'; then 
  echo $(echo -n "$1" | cut -c 2-);
else 
  echo $1;
fi
