#!/usr/bin/env sh

# If this is a pipeline of a version tag, also push this version 
# as latest to the registry alias $2:latest
if echo -n $1 | grep -Eq 'v[0-9]+\.[0-9]+\.[0-9]+'; then
  docker push $2:latest;
fi
