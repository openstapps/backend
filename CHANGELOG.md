# [1.0.0](https://gitlab.com/openstapps/backend/compare/v0.6.0...v1.0.0) (2023-05-08)


### Bug Fixes

* openapi docs generation ([4ebe44a](https://gitlab.com/openstapps/backend/commit/4ebe44a5a7a1b7bfd0aa5b84d47d4056d3068ffe))
* rename deprecated Gitlab CI variables ([3471591](https://gitlab.com/openstapps/backend/commit/3471591a7d458df70447c8dac91f96f3c83e763c))
* semster boosting ([515a6ee](https://gitlab.com/openstapps/backend/commit/515a6eeea56305a37510d99b9f84a6b118b66f8a))


### Features

* update to of elasticsearch 8.4 ([c9b83b5](https://gitlab.com/openstapps/backend/commit/c9b83b5d71610f82bd1d99e837e29ad445758aea))



# [0.6.0](https://gitlab.com/openstapps/backend/compare/v0.5.0...v0.6.0) (2023-01-30)



# [0.5.0](https://gitlab.com/openstapps/backend/compare/v0.4.1...v0.5.0) (2022-12-06)



## [0.4.1](https://gitlab.com/openstapps/backend/compare/v0.4.0...v0.4.1) (2022-11-02)



# [0.4.0](https://gitlab.com/openstapps/backend/compare/v0.3.1...v0.4.0) (2022-10-21)



## [0.3.1](https://gitlab.com/openstapps/backend/compare/v0.3.0...v0.3.1) (2022-09-02)



# [0.3.0](https://gitlab.com/openstapps/backend/compare/v0.2.0...v0.3.0) (2022-08-24)



# [0.2.0](https://gitlab.com/openstapps/backend/compare/v0.1.0...v0.2.0) (2022-08-22)


### Bug Fixes

* update PAIA API URL ([a20200e](https://gitlab.com/openstapps/backend/commit/a20200e52a725ede42cb5e026a5a693b1ba3d149))



# [0.1.0](https://gitlab.com/openstapps/backend/compare/16bbb7e9e36b7adf27452e1b09f7970e98aa27df...v0.1.0) (2022-06-30)


### Bug Fixes

* Add default configuration file for prometheus monitoring ([7ed2921](https://gitlab.com/openstapps/backend/commit/7ed2921efbce7fa5134206763eea986c6ef9a919))
* add prefix to schema names ([90822f5](https://gitlab.com/openstapps/backend/commit/90822f5888ed34d594b02df3f5cd9ec8ce74251e))
* apply correct types and tslint rules ([0cbf9b2](https://gitlab.com/openstapps/backend/commit/0cbf9b26a972563ad9483d78cea9ba809381f7dd))
* automatically remove invalid characters for aliases generated from index names ([6f7e23d](https://gitlab.com/openstapps/backend/commit/6f7e23df20af54abf2459da8aaebe236b82a5f1c))
* Don't force Mapping generation with 'npm start' ([4cfb560](https://gitlab.com/openstapps/backend/commit/4cfb560954ca97e22091e13d515475218885b1de))
* enhance default search query generation ([24a9122](https://gitlab.com/openstapps/backend/commit/24a91229f28fb17cb4941aab688a08266437c1eb))
* error thrown on consecutive connector executions ([2259da3](https://gitlab.com/openstapps/backend/commit/2259da317a848660fc2b0a14e50e2ae5ae329889)), closes [#73](https://gitlab.com/openstapps/backend/issues/73)
* esacpe mappin template filename ([496e6c5](https://gitlab.com/openstapps/backend/commit/496e6c5bd0563bd2dc0c6eff45c9bd685d95b453))
* fix markdown formatting in config ([9c5581a](https://gitlab.com/openstapps/backend/commit/9c5581af2cc131b81c288eadc39827124963ce55))
* fix reading url path as parameters ([9b889c8](https://gitlab.com/openstapps/backend/commit/9b889c873624d43baed4769a788db39528b143d8))
* imports from src in config files lead to crash ([f6003d7](https://gitlab.com/openstapps/backend/commit/f6003d7f8709d4424acd261cc7804e4d2684a9f4))
* invalid monthly cron execution time ([7a9f3ea](https://gitlab.com/openstapps/backend/commit/7a9f3eaca4667870ed19b17fd26c3c6d8e1fadd5))
* make config compatible with core, update database version ([f11376e](https://gitlab.com/openstapps/backend/commit/f11376ecf8c4997f6d0a85b3f3fde9e8b02bd61b))
* make facets work again ([d917627](https://gitlab.com/openstapps/backend/commit/d917627d588783d8734bd1d0c30d3d2782d02761))
* make index route work correctly ([fa2c9d7](https://gitlab.com/openstapps/backend/commit/fa2c9d7a881d9e94da8512349056b69206e4e3d0))
* properly check if an object exists before update ([e165837](https://gitlab.com/openstapps/backend/commit/e165837a154243305dc300acaa29605732290f6a)), closes [#70](https://gitlab.com/openstapps/backend/issues/70)
* remove onlyOnTypes from mustMatch ([1d5f99b](https://gitlab.com/openstapps/backend/commit/1d5f99b1fa74c486dc4df29daf9c0aa453935821)), closes [#83](https://gitlab.com/openstapps/backend/issues/83)
* replace isProductiveEnvironment with isTestEnvironment ([8c48552](https://gitlab.com/openstapps/backend/commit/8c48552abf7b7983e966ebc22299b334cae46167))
* return syntax error when receiving bad json ([12b71ba](https://gitlab.com/openstapps/backend/commit/12b71ba1f1f3e45a84b001b395dcfa1578355276)), closes [#3](https://gitlab.com/openstapps/backend/issues/3)
* return validation error instead of internal server error ([24e27c1](https://gitlab.com/openstapps/backend/commit/24e27c1d9e002db3b8f4afb4a31ad994c0eaad42))
* route for news ([7e35fae](https://gitlab.com/openstapps/backend/commit/7e35fae34e02bf2e12cf0d0b960d48de97fd4200))
* set config file before accessing it ([e17db52](https://gitlab.com/openstapps/backend/commit/e17db521e2a3f4f15dc5e5b758bd1ada10c78161)), closes [#27](https://gitlab.com/openstapps/backend/issues/27)
* stapps core version in config ([32c8a21](https://gitlab.com/openstapps/backend/commit/32c8a2149ad18179610af02483b794b426b7b9dc)), closes [#74](https://gitlab.com/openstapps/backend/issues/74)
* take coordinates in right order ([bb3be5a](https://gitlab.com/openstapps/backend/commit/bb3be5a816f7e4eb85b69ec558572bed9c3b6b39)), closes [#116](https://gitlab.com/openstapps/backend/issues/116)
* use SCRequestBodyTooLargeError ([e70e50a](https://gitlab.com/openstapps/backend/commit/e70e50a1eab00cd180479c5e0299f974da92fe94)), closes [#20](https://gitlab.com/openstapps/backend/issues/20)
* use specific time from filter if defined ([80e6249](https://gitlab.com/openstapps/backend/commit/80e62496f0731af721193875d5a6cdbf10b34607))
* wait for config file validation ([30082f8](https://gitlab.com/openstapps/backend/commit/30082f87262fa7428172fcbb6710b66d4bfe6261))
* wrong way alias names are generated ([9488451](https://gitlab.com/openstapps/backend/commit/94884510802906cdbfdd3490fac5fc9401937da7)), closes [#79](https://gitlab.com/openstapps/backend/issues/79)


### Features

* add backend ([16bbb7e](https://gitlab.com/openstapps/backend/commit/16bbb7e9e36b7adf27452e1b09f7970e98aa27df))
* add boosting to context based search ([dd4be92](https://gitlab.com/openstapps/backend/commit/dd4be92f90c6e7047eea0d00a2442b75673329f6))
* add catalog menu entry and rearrange ([4e42772](https://gitlab.com/openstapps/backend/commit/4e42772ca3788d3ee7af78266ee223e05fe6a8f3))
* add default app settings and menus ([54301ae](https://gitlab.com/openstapps/backend/commit/54301ae8fb656db17e231197c3a64fc27f541024))
* add dummy about config ([d6f126f](https://gitlab.com/openstapps/backend/commit/d6f126f19776ee7902e65d9248501b4986657ed9))
* add favorites to personal menu ([de0617b](https://gitlab.com/openstapps/backend/commit/de0617b8dd51182326094543110b8ed294a6e940))
* add feedback to config as menu item ([7ba6472](https://gitlab.com/openstapps/backend/commit/7ba647271efd87c422ce302bd5baf5760671e1a1))
* add functionality to register plugins via http ([3d51ccf](https://gitlab.com/openstapps/backend/commit/3d51ccfac26b2c3ad1271c29c1e736aaf7fcd88f)), closes [#2](https://gitlab.com/openstapps/backend/issues/2) [#37](https://gitlab.com/openstapps/backend/issues/37)
* add hebis proxy url ([ca1d244](https://gitlab.com/openstapps/backend/commit/ca1d2444e03aacad37219d93d657968b7a933f78)), closes [#120](https://gitlab.com/openstapps/backend/issues/120)
* add openapi docs generation to api ([614a1b1](https://gitlab.com/openstapps/backend/commit/614a1b1e9b3c525d5524065851689f793a4b3b4b))
* Add prometheus middleware to express ([b42e911](https://gitlab.com/openstapps/backend/commit/b42e911a117c59b2d70c1587f9e9b20a846e92d0))
* add routes doc generation to npm documentation script ([4a64f26](https://gitlab.com/openstapps/backend/commit/4a64f26e43979cdd4390bad796e1b7dc3f8c7027))
* add schedule route ([785813c](https://gitlab.com/openstapps/backend/commit/785813c3fb92bbebd7b07246bbcec77a9a4520e3))
* Add start-debug npm run script ([23eb1e2](https://gitlab.com/openstapps/backend/commit/23eb1e2263d2dd9d55f9a6ac5fca2a8fad7f1e84))
* add support for generated elasticsearch mappings ([8eab6b8](https://gitlab.com/openstapps/backend/commit/8eab6b8531d91d4569fe13743624f5ce91a7dc1e)), closes [#38](https://gitlab.com/openstapps/backend/issues/38)
* add support for multiple values in value filter ([de60311](https://gitlab.com/openstapps/backend/commit/de60311bd072db01eaf50965f3fdc307bfedc4c2))
* add support for new availability filter ([47f3232](https://gitlab.com/openstapps/backend/commit/47f3232f155a13ea39abb2aaaecafd54fa7d98ce))
* add support for range filters ([dc16974](https://gitlab.com/openstapps/backend/commit/dc169746e7fa0f50a1f969653043e5a4d5fa0f01))
* add support for range filters ([dcf7906](https://gitlab.com/openstapps/backend/commit/dcf7906f79f07d9cfee704454eedddb452a1f255))
* adjust to changes of SCFacet in core v0.12.0 ([2f13010](https://gitlab.com/openstapps/backend/commit/2f13010480b87a4a06634084033b3b0822eb78ee)), closes [#30](https://gitlab.com/openstapps/backend/issues/30)
* allow for searching illegal elasticsearch characters ([b629d05](https://gitlab.com/openstapps/backend/commit/b629d058eb64c7a785648fab0a7073eeaa37e895))
* boost academic terms dynamically ([13938ec](https://gitlab.com/openstapps/backend/commit/13938ecf211060a3f50747ac7fed343b71a6aa4b))
* log registration and removal of plugins ([006bbeb](https://gitlab.com/openstapps/backend/commit/006bbebe60b7f5015d80ac825622cefbc25cd959)), closes [#71](https://gitlab.com/openstapps/backend/issues/71)
* make backend work with automatically generated aggregations ([ba2c6f6](https://gitlab.com/openstapps/backend/commit/ba2c6f655c263f0b1d1e55b739adde0004bbf408))
* move EXTERNAL_REQUEST_TIMEOUT to config file ([5d6d4b5](https://gitlab.com/openstapps/backend/commit/5d6d4b53f01550365a330daecbcb4e0f6bdb8aef))
* move up and enable cors ([6483221](https://gitlab.com/openstapps/backend/commit/6483221b62d84c5871ee4ec82158f8b9ef1ed54e))
* support geo shape filter ([dd8a6b3](https://gitlab.com/openstapps/backend/commit/dd8a6b3abcc90c50a16167054e2f3cdcee40c863))
* use config file for maxRequestBodySize ([d110d60](https://gitlab.com/openstapps/backend/commit/d110d60123a28563fe0c58ab903f61255a9644a2))
* use config for MultiSearchRoute ([8278279](https://gitlab.com/openstapps/backend/commit/827827905b71c71dfa9299a9deb92a8c9cfc0e20))
* use new Elasticsearch package ([1bad092](https://gitlab.com/openstapps/backend/commit/1bad092185d989078badc66f9c138d2e1baa27e4))
* utilize api-cli for e2e integration test ([ce06e73](https://gitlab.com/openstapps/backend/commit/ce06e735bea379c9ad955a627e3e1cead37c85fe))



