FROM registry.gitlab.com/openstapps/projectmanagement/node

USER root
RUN apk add --update python3 py3-pip make g++

USER node
ADD --chown=node:node . /app
WORKDIR /app
RUN npm ci && npm run build

EXPOSE 3000

CMD ["node", "./lib/cli"]
