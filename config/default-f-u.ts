// tslint:disable:no-default-export
// tslint:disable:no-magic-numbers
import {SCAboutPageContentType, SCConfigFile, SCLanguageCode} from '@openstapps/core';
import {RecursivePartial} from '@openstapps/logger/lib/common';

const markdownSources: {
  [key in SCLanguageCode]?: {
    /**
     * Privacy policy markdown
     */
    privacyPolicy?: string;
    /**
     * Terms and conditions markdown
     */
    termsAndConditions?: string;
  };
} = {en: {}, de: {}};

markdownSources.de!.privacyPolicy = `
# Datenschutzerklärung

## Kontaktdaten des Verantwortlichen

Verantwortlich im Sinne der Datenschutz-Grundverordnung und weiterer Vorschriften zum Datenschutz ist die:

Johann Wolfgang Goethe-Universität Frankfurt am Main vertreten durch ihren Präsidenten<br />
Theodor-W.-Adorno-Platz 1<br />
60323 Frankfurt am Main

Postanschrift:<br />
Goethe-Universität Frankfurt am Main<br />
60629 Frankfurt

Website: http://www.uni-frankfurt.de

## Kontaktdaten der Datenschutzbeauftragten an der Goethe-Universität

Sie erreichen die behördlichen Datenschutzbeauftragten der Johann Wolfgang Goethe-Universität Frankfurt am Main unter:<br />
Mail: <dsb@uni-frankfurt.de><br />
Website: http://www.uni-frankfurt.de/47859992/datenschutzbeauftragte

## Informationen zur Verarbeitung personenbezogener Daten

### <u>1. Umfang der Verarbeitung personenbezogener Daten</u>

Personenbezogene Daten sind gemäß Artikel 4 DSGVO alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person beziehen.

Wir verarbeiten personenbezogene Daten von Ihnen als Nutzer:innen der Goethe-Uni-App, soweit dies zur Bereitstellung einer **funktionsfähigen Applikation** technisch erforderlich ist.

Weiterhin kann eine Datenverarbeitung auf Ihrer freiwilligen Einwilligung basieren, wenn Sie **spezifische Funktionen** nutzen möchten.

Wir unterscheiden daher nachfolgend zwischen

-   Zugriffsdaten bei der Nutzung der App: Inhalt der Anfragen, IP-Adressen, Datum/Uhrzeit der Anfrage, Angefragte URL, Fehlermeldungen, Browser-Kennung, HTTP-Header

-   Standortbestimmung und Navigation: freiwillige Standortangaben

-   Nutzer:inneneinstellungen: freiwillige Angabe von a) Sprachpräferenzen (derzeit: deutsch/englisch), b) Status (z. B. Gast/Student) oder c) spezifischen Suchanfragen und Suchergebnissen (Notifications)

-   Kalenderfunktion: freiwillige Nutzung der Kalenderfunktion (optional mit freiwilliger Nutzung einer Synchronisationsfunktion: Opt-in) oder der integrierten Stundenplanfunktion, hierbei werden folgende Daten auf dem Endgerät verarbeitet und gespeichert: Termine und Veranstaltungen

-   Feedbackfunktion und Kontaktaufnahme: freiwillige Nutzung mit der Angabe von Kontaktdaten und ggf. freiwilliger Übermittlung von Protokolldaten

-   Campus Dienste: freiwillige Nutzung mit Verarbeitung von Notenansicht, Matrikelnummer, E-Mailadresse, Name

-   Funktionen der Bibliothek: freiwillige Nutzung mit Verarbeitung von Bibliothekskontodaten, wie z.B. Ausweisnummer mit Name, E-Mailadresse, postalischer Adresse, Nutzungsberechtigung, Bestelldaten, Gebühren, Vormerkung, Ausleihdaten. Die vollständigen Angaben zur Verarbeitung finden Sie in der Datenschutzerklärung der Bibliothek:<br />
    https://www.ub.uni-frankfurt.de/benutzung/datenschutz.html

Die App verlinkt an einigen Stellen auf die Website der Goethe-Universität sowie auf andere, externe Websites, die in einem In-App-Browser dargestellt werden. Wir bitten Sie bei Aufruf dieser Websites, die dort geltenden gesonderte Datenschutzhinweise und Erklärungen zu beachten.

### <u>2. Zweck(e) der Datenverarbeitung</u>

**Zugriff auf Standortdaten**

Für die Navigation benötigt die Goethe-Uni-App Zugriff auf den Standort des verwendeten Endgerätes (Location Based Services). Bei einer Anfrage erhebt die App den aktuellen Standort über GPS, Funkzellendaten und WLAN-Datenbanken, um Ihnen als Nutzer:in Informationen zu Ihrer unmittelbaren Umgebung geben zu können. Der Zugriff auf die Standortdaten erfolgt nur, wenn Sie den Zugriff auf die Standortdaten erlauben. Daten zu Ihrem Standort werden ausschließlich für die Bearbeitung von standortbezogenen Anfragen genutzt und um Ihren Standort auf der Karte anzuzeigen.

**Zugriff auf Zugriffsdaten**

Die Speicherung und Verarbeitung von Protokolldateien erfolgt, um die Funktionsfähigkeit der Goethe Uni-App für Sie sicherzustellen. Zudem benötigen wir die die Daten aus Gründen der Sicherheit unserer informationstechnischen Systeme. Eine anderweitige Auswertung oder Weitergabe findet in diesem Zusammenhang nicht statt.

**Zugriff auf Spracheinstellungen**

Der Zugriff auf die Spracheinstellung erfolgt um Ihnen die Oberfläche der App in der von Ihnen gewünschten Sprache anzuzeigen.

**Zugriff auf die Einstellung der Statusgruppe**

Der Zugriff auf die Einstellung der Statusgruppe erfolgt um Ihnen in der App die für Ihre Gruppe zutreffenden Informationen anzuzeigen, z.B. Mensapreise

**Zugriff auf personenbezogene Daten bei der Nutzung der Feedbackfunktion**

Die Verarbeitung der personenbezogenen Daten aus der Feedbackfunktion dient uns zur Kontaktaufnahme und Fehlerbehebung.

**Zugriff auf personenbezogene Daten bei der Kalendersynchronisation**

Der Zugriff auf die Termindaten erfolgt um sie bei aktivierter Kalenderfunktion in den Gerätekalender zu schreiben.

**Zugriff auf Daten der Campus Dienste**

Der Zugriff auf das Campus Management Systems erfolgt ausschließlich um persönliche Daten der Studierendenverwaltung in der App anzuzeigen (z.B. Prüfungsnoten).

**Zugriff auf bibliotheksspezifische personenbezogene Daten**

Der Zugriff auf die Daten (z.B. Ausweisnummer, Name, Postanschrift) erfolgt zur Durchführung von Bestell- und Ausleihverfahren von Büchern und sonstigen Materialien der Universitätsbibliothek. Die vollständigen Angaben zu den Verarbeitungszwecken finden Sie in der Datenschutzerklärung der Bibliothek: https://www.ub.uni-frankfurt.de/benutzung/datenschutz.html

### <u>3. Rechtsgrundlage(n) für die Datenverarbeitung</u>

Die Nutzung der Nutzungs-/Zugriffsdaten („Protokolldateien") basiert auf Artikel 6 Absatz 1 lit. f) DSGVO.

Für alle spezifischen Funktionen, bei denen die Datenverarbeitung auf Ihrer freiwilligen Einwilligung als Nutzer:innen basiert, werden explizit Einwilligungen bzw. aktive Zustimmungsakte („Opt-In") eingeholt. Die Bereitstellung personenbezogener Daten zu Ihrer Person gegenüber der Goethe-Universität erfolgen dabei auf freiwilliger Basis. Die Rechtsgrundlage ist in diesen Fällen jeweils Artikel 6 Absatz 1 lit. a) DSGVO. Sie können Ihre jeweilige Einwilligung jederzeit einzeln widerrufen bzw. Ihre Einstellungen ändern.

### <u>4. Datenlöschung und Speicherdauer</u>

Die in den Protokolldateien der App erfassten Daten werden sieben Tage nach dem Ende des Zugriffs automatisch gelöscht oder anonymisiert.

Die Löschfristen bzw. Speicherdauer der in den Bibliotheksystemen erfassten Daten finden Sie in der Datenschutzerklärung der Bibliothek: https://www.ub.uni-frankfurt.de/benutzung/datenschutz.html

Für alle anderen Funktionen und Dienste gilt: Die Löschung erfolgt hier je nach Vorgabe des genutzten Dienstes. Die personenbezogenen Daten der betroffenen Person werden gelöscht oder gesperrt, sobald der Zweck der Speicherung entfällt.

### <u>5. Datenweitergabe/Datenübermittlung</u>

Ihre personenbezogenen Daten werden von uns nicht an Dritte weitergegeben. 

Von Betreiberseite wird durch technische und organisatorische Maßnahmen sichergestellt, dass Dritte keinen Zugriff auf die verarbeiteten Daten, wie z. B. Nutzungsdaten, erhalten. Ein Auftragsverarbeitungsverhältnis nach Art. 28 DSGVO besteht nicht, da ausschließlich eigene Server verwendet werden.

### <u>6. Automatisierte Entscheidungsfindung</u>

Eine automatisierte Entscheidungsfindung einschließlich Profiling erfolgt nicht.

## Rechte der betroffenen Person

Werden personenbezogene Daten von Ihnen verarbeitet, sind Sie Betroffener im Sinne der DSGVO. Die Geltendmachung Ihrer Betroffenenrechte ist kostenfrei. Sie können sich dafür selbstverständlich an uns wenden. Es stehen Ihnen folgende Betroffenenrechte gegenüber der Goethe-Universität zu:

### <u>1. Auskunftsrecht</u>

Sie können von uns als verantwortlicher Stelle eine Bestätigung darüber verlangen, ob und welche Ihrer personenbezogenen Daten von uns verarbeitet werden. Sie haben das Recht, von uns Kopien Ihrer personenbezogenen Daten zu verlangen. Bitte beachten Sie die Ausnahmen, die sich durch spezifische Vorschriften ergeben können.

### <u>2. Recht auf Berichtigung</u>

Sie haben das Recht von uns die Berichtigung und/oder Vervollständigung zu verlangen, sofern die verarbeiteten personenbezogenen Daten, die Sie betreffen, nicht (mehr) richtig oder nicht (mehr) vollständig sind.

### <u>3. Recht auf Einschränkung der Verarbeitung</u>

Unter bestimmten Voraussetzungen können Sie die Einschränkung der Verarbeitung der Sie betreffenden personenbezogenen Daten verlangen, d. h. dass dann Ihre personenbezogenen Daten zwar nicht gelöscht, aber gekennzeichnet werden, so dass eine weitere Verarbeitung eingeschränkt ist.

### <u>4. Recht auf Löschung</u>

Sie können unter bestimmten Voraussetzungen von uns verlangen, dass die Sie betreffenden personenbezogenen Daten unverzüglich gelöscht werden. Dies ist insbesondere der Fall, wenn die personenbezogenen Daten zu dem Zweck, zu dem sie ursprünglich erhoben oder verarbeitet wurden, nicht mehr erforderlich sind.

### <u>5. Recht auf Unterrichtung</u>

Haben Sie das Recht auf Berichtigung, Löschung oder Einschränkung der Verarbeitung uns gegenüber geltend gemacht, sind wir verpflichtet, allen Empfänger/innen, denen die Sie betreffenden personenbezogenen Daten offengelegt wurden, diese Berichtigung oder Löschung der Daten oder Einschränkung der Verarbeitung mitzuteilen, es sei denn, dies erweist sich als unmöglich oder ist mit einem unverhältnismäßigen Aufwand verbunden. Sie sind berechtigt, über diese Empfänger unterrichtet zu werden.

### <u>6. Recht auf Datenübertragbarkeit</u>

Sie haben unter bestimmten Voraussetzungen das Recht von uns zu verlangen, dass Ihre personenbezogenen Daten von uns direkt an einen anderen Verantwortlichen oder an eine andere Organisation übermittelt werden. Alternativ haben Sie unter bestimmten Voraussetzungen das Recht von uns zu verlangen, dass wir Ihnen selbst die Daten in einem maschinenlesbaren Format bereitstellen.

### <u>7. Widerspruchsrecht</u>

Wenn wir Ihre personenbezogenen Daten verarbeiten, weil die Verarbeitung im öffentlichen Interesse, Teil unserer öffentlichen Aufgaben ist bzw. wenn wir Ihre Daten auf Basis eines berechtigten Interesses verarbeiten, haben Sie aus Gründen, die sich aus Ihrer besonderen Situation ergeben, das Recht, jederzeit der Verarbeitung der Sie betreffenden Daten zu widersprechen.

### <u>8. Recht auf Widerruf der datenschutzrechtlichen Einwilligungserklärung</u>

Wenn wir Ihre personenbezogenen Daten verarbeiten, weil Sie uns Ihre Einwilligung gegeben haben, haben Sie jederzeit das Recht, Ihre Einwilligungserklärung zu widerrufen.

### <u>9. Recht auf Beschwerde bei einer Aufsichtsbehörde</u>

Sie haben ferner das Recht auf Beschwerde bei einer Aufsichtsbehörde. Die zuständige Aufsichtsbehörde wird Ihre Beschwerde prüfen.

## **Kontaktdaten der Aufsichtsbehörde im Bereich Datenschutz**

Wenn Sie der Ansicht sind, dass eine Verarbeitung der Sie betreffenden personenbezogenen Daten gegen Datenschutzvorschriften verstößt, wenn Sie eine allgemeine Anfrage haben oder wenn Sie sich bei einer zuständigen Fachaufsichtsbehörde beschweren wollen, können Sie sich an den Hessischen Beauftragten für Datenschutz und Informationsfreiheit (HBDI) wenden.

**Der Hessische Beauftragte für Datenschutz und Informationsfreiheit ist auf unterschiedlichen Wegen erreichbar:**

<u>**Der Hessische Beauftragte für Datenschutz und Informationsfreiheit**</u><br />
Postfach 3163<br />
65021 Wiesbaden

Telefon: +49 611 1408 -- 0

Für allgemeine Anfragen können Sie ein Kontaktformular nutzen:<br />
<https://datenschutz.hessen.de/kontakt><br />
<br />
Für Beschwerden steht Ihnen zudem ein Beschwerdeformular zur Verfügung:<br />
<https://datenschutz.hessen.de/service/beschwerde-uebermitteln>
`;

markdownSources.en!.privacyPolicy = `
# Privacy policy

## Contact details of the person responsible

Responsible in the sense of the General Data Protection Regulation and further regulations on data protection is the:

Johann Wolfgang Goethe-Universität Frankfurt am Main represented by its president<br />
Theodor-W.-Adorno-Platz 1<br />
60323 Frankfurt am Main

Postanschrift:<br />
Goethe-Universität Frankfurt am Main<br />
60629 Frankfurt

Website: http://www.uni-frankfurt.de

## Contact details of the data protection officer at Goethe University

You can reach the data protection officers at Johann Wolfgang Goethe University Frankfurt am Main at:<br />
Mail: <dsb@uni-frankfurt.de><br />
Website: http://www.uni-frankfurt.de/47859992/datenschutzbeauftragte

## Information on the processing of personal data

### <u>1. Scope of the processing of personal data</u>.

According to Article 4 DSGVO, personal data is any information relating to an identified or identifiable natural person.

We process personal data of you as a user inside of the Goethe University App to the extent that this is technically necessary for the provision of a **functional application**.

Furthermore, data processing may be based on your voluntary consent if you wish to use **specific functions**.

We therefore distinguish below between

- Access data when using the app: content of requests, IP addresses, date/time of request, requested URL, error codes, browser identifier, HTTP header.

- Location and navigation: voluntary location information

- User settings: voluntary specification of a) language preferences (currently: German/English), b) status (e.g. guest/student) or c) specific search queries and search results (notifications)

- Calendar function: voluntary use of the calendar function (optional with voluntary use of a sync function: opt-in) or the integrated timetable function. The following data is processed and stored on the users device: appointments and events

- Feedback function and contacting: voluntary use with the provision of contact data and, if applicable, voluntary transmission of log data

- Campus services: voluntary use with processing of grade view, matriculation number, email address, name

- Services of the library: voluntary use with processing of library account data, such as ID number with name, e-mail address, postal address, right of use, order data, fees, reservation, loan data. Full details of processing can be found in the library's privacy policy:<br />
    https://www.ub.uni-frankfurt.de/benutzung/datenschutz.html

In some places, the app links to the Goethe University website and to other external websites that are displayed in an in-app browser. When you visit these websites, we ask you to pay attention to the separate data protection notices and declarations that apply there.

### <u>2. Purpose(s) of data processing</u>

**Access to location data**.

For navigation, the Goethe University app requires access to the location of the end device used (location-based services). When a request is made, the app collects the current location via GPS, radio cell data and WLAN databases in order to be able to give you as a user:in information about your immediate surroundings. The location data is only accessed if you allow access to the location data. Data about your location is only used to process location-related requests and to display your location on the map.

**Access to access data**.

Log files are stored and processed to ensure that the Goethe Uni app functions properly for you. In addition, we need the data for reasons of security of our information technology systems. No other evaluation or disclosure takes place in this context.

**Access to language settings**.

Access to the language setting is made in order to display the interface of the app in the language of your choice.

**Access to the status group setting**.

Access to the status group setting is provided to show you the information in the app that applies to your group, e.g. canteen prices.

**Access to personal data when using the feedback function**.

We use the processing of personal data from the feedback function to contact you and troubleshoot problems.

**Access to personal data when synchronizing calendars**.

Appointment data is accessed in order to write it to the device calendar when the calendar function is enabled.

**Access to Campus Services data**.

Access to the Campus Management System is solely for the purpose of displaying personal student management data in the app (e.g., exam grades).

**Access to library-specific personal data**.

Access to data (e.g., ID number, name, mailing address) is for the purpose of carrying out ordering and borrowing procedures for books and other materials from the University Library. Full details of the purposes of processing can be found in the Library's Privacy Policy: https://www.ub.uni-frankfurt.de/benutzung/datenschutz.html

### <u>3. Rechtsgrundlage(n) für die Datenverarbeitung</u>

The use of usage/access data ("log files") is based on Article 6(1)(f) DSGVO.

For all specific functions where data processing is based on your voluntary consent as a user:in, explicit consent or active acts of consent ("opt-in") are obtained. The provision of personal data about you to Goethe University is done on a voluntary basis. The legal basis in each of these cases is Article 6 (1) a) DSGVO. You can individually revoke your respective consent or change your settings at any time.

### <u>4. Data deletion and storage duration</u>

The data collected in the log files of the app are automatically deleted or anonymized seven days after the end of the access.

The deletion periods or storage duration of the data collected in the library systems can be found in the library's privacy policy: https://www.ub.uni-frankfurt.de/benutzung/datenschutz.html

For all other functions and services, the following applies: deletion takes place here depending on the specifications of the service used. The personal data of the data subject will be deleted or blocked as soon as the purpose of the storage no longer applies.

### <u>5. Data disclosure/data transfer</u>

We will not pass on your personal data to third parties. 

On the part of the operator, technical and organizational measures are taken to ensure that third parties do not gain access to the processed data, such as usage data. An order processing relationship according to Art. 28 DSGVO does not exist, as only our own servers are used.

### <u>6. Automated decision-making</u>

Automated decision-making, including profiling, does not take place.

## Rights of the data subject

If personal data is processed by you, you are a data subject within the meaning of the GDPR. The assertion of your data subject rights is free of charge. You can, of course, contact us for this purpose. You are entitled to the following data subject rights vis-à-vis Goethe University:

### <u>1. Right of access</u>

You can request confirmation from us as the controller as to whether and which of your personal data is being processed by us. You have the right to request copies of your personal data from us. Please note the exceptions that may arise due to specific regulations.

### <u>2. Right of rectification</u>

You have the right to request us to rectify and/or complete, if the processed personal data concerning you is not (anymore) accurate or not (anymore) complete.

### <u>3. Right to restriction of processing</u>

Under certain conditions, you can request the restriction of the processing of personal data concerning you, i.e. that your personal data is then not deleted, but marked so that further processing is restricted.

### <u>4. Right to erasure</u>

Under certain conditions, you can demand that we delete the personal data concerning you without delay. This is particularly the case if the personal data is no longer necessary for the purpose for which it was originally collected or processed.

### <u>5. Right to information</u>

If you have asserted the right to rectification, erasure or restriction of processing against us, we are obliged to inform all recipients to whom the personal data concerning you have been disclosed of this rectification or erasure of the data or restriction of processing, unless this proves impossible or involves a disproportionate effort. You are entitled to be informed about these recipients.

### <u>6. Right to data portability</u>

Under certain conditions, you have the right to request that we transfer your personal data directly to another controller or organization. Alternatively, under certain conditions, you have the right to request that we ourselves provide you with the data in a machine-readable format.

### <u>7. Right to object</u>

If we process your personal data because the processing is in the public interest, part of our public duties, or if we process your data on the basis of a legitimate interest, you have the right to object at any time to the processing of data relating to you for reasons arising from your particular situation.

### <u>8. Right to revoke the declaration of consent under data protection law</u>

If we process your personal data because you have given us your consent, you have the right to revoke your declaration of consent at any time.

### <u>9. Right to lodge a complaint with a supervisory authority</u>

You also have the right to lodge a complaint with a supervisory authority. The competent supervisory authority will examine your complaint.

## **Contact details of the supervisory authority in the area of data protection**

If you believe that the processing of your personal data violates data protection regulations, if you have a general inquiry or if you want to complain to a competent supervisory authority, you can contact the Hessian Commissioner for Data Protection and Freedom of Information (HBDI).

**The Hessian Commissioner for Data Protection and Freedom of Information can be reached in different ways:**

<u>**The Hessian Commissioner for Data Protection and Freedom of Information**</u><br />
PO Box 3163<br />
65021 Wiesbaden

Telephone: +49 611 1408 -- 0

For general inquiries you can use a contact form:<br />
<https://datenschutz.hessen.de/kontakt><br />
<br />
A complaint form is also available for complaints:<br />
<https://datenschutz.hessen.de/service/beschwerde-uebermitteln>
`;

/**
 * This is the default configuration for the Goethe university of Frankfurt
 */
const config: RecursivePartial<SCConfigFile> = {
  auth: {
    default: {
      client: {
        clientId: '1cac3f99-33fa-4234-8438-979f07e0cdab',
        scopes: '',
        url: 'https://cas.rz.uni-frankfurt.de/cas/oauth2.0',
      },
      endpoints: {
        authorization: 'https://cas.rz.uni-frankfurt.de/cas/oauth2.0/authorize',
        endSession: 'https://cas.rz.uni-frankfurt.de/cas/logout',
        mapping: {
          id: '$.id',
          email: '$.attributes.mailPrimaryAddress',
          familyName: '$.attributes.sn',
          givenName: '$.attributes.givenName',
          name: '$.attributes.givenName',
          role: '$.attributes.eduPersonPrimaryAffiliation',
          studentId: '$.attributes.employeeNumber',
        },
        token: 'https://cas.rz.uni-frankfurt.de/cas/oauth2.0/accessToken',
        userinfo: 'https://cas.rz.uni-frankfurt.de/cas/oauth2.0/profile',
      },
    },
    paia: {
      client: {
        clientId: '',
        scopes: '',
        url: 'https://hds.hebis.de/Shibboleth.sso/UBFFM?target=https://hds.hebis.de/ubffm/paia_login_stub.php',
      },
      endpoints: {
        authorization:
          'https://hds.hebis.de/Shibboleth.sso/UBFFM?target=https://hds.hebis.de/ubffm/paia_login_stub.php',
        endSession: 'https://ubffm.hds.hebis.de/Shibboleth.sso/Logout',
        mapping: {
          id: '$.email',
          name: '$.name',
          role: '$.type',
        },
        token: 'https://hds.hebis.de/paia/auth/login',
        userinfo: 'https://hds.hebis.de/paia/core',
      },
    },
  },
  app: {
    features: {
      extern: {
        hisometry: {
          authProvider: 'default',
          url: 'https://his-self-service.rz.uni-frankfurt.de',
        },
        daia: {
          url: 'https://daia.hebis.de/DAIA2/UB_Frankfurt',
        },
        hebisProxy: {
          url: 'https://proxy.ub.uni-frankfurt.de/login?qurl=',
        },
        paia: {
          authProvider: 'paia',
          url: 'https://hds.hebis.de/paia/core',
        },
      },
    },
    aboutPages: {
      'about': {
        title: 'Über Open StApps',
        content: [
          {
            title: 'Verbundprojekt mehrerer Hochschulen für eine generische Studierenden-App',
            content: {
              type: SCAboutPageContentType.MARKDOWN,
              value: `
              Open StApps bietet Studierenden aller beteiligten Hochschulen eine qualitativ
              hochwertige App für den Studienalltag. Open StApps-Verbundpartner integrieren
          generalisierbare Studierendenprozesse so in App-Module, dass diese auch
              von anderen Hochschulen verwendet werden können. Die in der Open StApps App 
              verwendeten Daten einer Datenquelle sind in einem generalisierten Datenmodell
              so aufbereitet, dass ein Austausch oder Abschaltung der Datenquelle problemlos möglich 
              ist und die Open StApps App problemlos weiterhin funktionsfähig bleibt.
          `,
              translations: {
                en: {
                  value: `Open StApps provides students from all participating universities with a
                  high-quality app for everyday study. Open StApps partners integrate
                  generalizable student processes into app modules in such a way that they can be
                  can be used by other universities. The data of a data source used in the Open StApps app 
                  is prepared in a generalized data model in a way that the data source can be easily 
                  exchanged or switched off while the app continues to function without any problems.
                  `,
                },
              },
            },
            translations: {
              en: {
                title: 'Collaborative project of multiple universities for a single generic study app',
              },
            },
            type: SCAboutPageContentType.SECTION,
          },
          {
            title: 'Goethe-Uni Kontakt',
            content: {
              rows: [
                [
                  {
                    value: 'Adresse',
                    translations: {
                      en: {
                        value: 'Address',
                      },
                    },
                    type: SCAboutPageContentType.MARKDOWN,
                  },
                  {
                    value: `
                Goethe Universität<br>
                Hochschulrechenzentrum (HRZ)<br>
                Norbert-Wollheim-Platz 1<br>
                60629 Frankfurt
                `,
                    translations: {},
                    type: SCAboutPageContentType.MARKDOWN,
                  },
                ],
                [
                  {
                    value: 'Kontaktinformation',
                    translations: {
                      en: {
                        value: 'Contact information',
                      },
                    },
                    type: SCAboutPageContentType.MARKDOWN,
                  },
                  {
                    value:
                      '[app@rz.uni-frankfurt.de](mailto:app@rz.uni-frankfurt.de)<br>' +
                      '[+49 69 798 32936](tel:+496979832936)<br>' +
                      '[https://app.rz.uni-frankfurt.de](https://app.rz.uni-frankfurt.de)',
                    translations: {},
                    type: SCAboutPageContentType.MARKDOWN,
                  },
                ],
              ],
              type: SCAboutPageContentType.TABLE,
            },
            translations: {
              en: {
                title: 'Goethe-Uni Contact',
              },
            },
            type: SCAboutPageContentType.SECTION,
          },
          {
            icon: 'newspaper',
            title: 'Neue Funktionen / Gelöste Probleme',
            link: 'changelog',
            translations: {
              en: {
                title: 'New features / Resolved issues',
              },
            },
            type: SCAboutPageContentType.ROUTER_LINK,
          },
          {
            icon: 'description',
            title: 'Impressum',
            link: 'imprint',
            translations: {
              en: {
                title: 'Imprint',
              },
            },
            type: SCAboutPageContentType.ROUTER_LINK,
          },
          {
            icon: 'policy',
            title: 'Datenschutz',
            link: 'privacy',
            translations: {
              en: {
                title: 'Privacy policy',
              },
            },
            type: SCAboutPageContentType.ROUTER_LINK,
          },
          {
            icon: 'copyright',
            title: 'Bibliotheken und Lizenzen',
            link: 'licenses',
            translations: {
              en: {
                title: 'Libraries and licenses',
              },
            },
            type: SCAboutPageContentType.ROUTER_LINK,
          },
        ],
        translations: {
          en: {
            title: 'About Open StApps',
          },
        },
      },
      'about/imprint': {
        title: 'Impressum',
        content: [
          {
            value: `
                [Impressum der Johann Wolfgang Goethe-Universität Frankfurt am Main](https://www.uni-frankfurt.de/impressum)
              `,
            translations: {
              en: {
                value: `
                    [Imprint of the Goethe University Frankfurt](https://www.uni-frankfurt.de/impressum)
                  `,
              },
            },
            type: SCAboutPageContentType.MARKDOWN,
          },
        ],
        translations: {
          en: {
            title: 'Imprint',
          },
        },
      },
      'about/privacy': {
        title: 'Datenschutz',
        content: [
          {
            value: markdownSources.de!.privacyPolicy,
            translations: {
              en: {
                value: markdownSources.en!.privacyPolicy,
              },
            },
            type: SCAboutPageContentType.MARKDOWN,
          },
        ],
        translations: {
          en: {
            title: 'Privacy Policy',
          },
        },
      },
    },
  },
};

export default config;
