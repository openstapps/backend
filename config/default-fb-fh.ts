// tslint:disable:no-default-export
// tslint:disable:no-magic-numbers
import {SCConfigFile} from '@openstapps/core';
import {RecursivePartial} from '@openstapps/logger/lib/common';

/**
 * This is the default configuration for the university of Kassel
 */
const config: RecursivePartial<SCConfigFile> = {};

export default config;
