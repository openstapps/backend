// tslint:disable:no-default-export
// tslint:disable:no-magic-numbers
import {RecursivePartial} from '@openstapps/logger/lib/common';
import {ElasticsearchConfigFile} from '../src/storage/elasticsearch/types/elasticsearch';

/**
 * This is the database configuration for the technical university of berlin
 */
const config: RecursivePartial<ElasticsearchConfigFile> = {
  internal: {
    database: {
      name: 'elasticsearch',
      query: {
        minMatch: '60%',
        queryType: 'query_string',
      },
    },
  },
};

export default config;
