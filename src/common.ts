/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {SCConfigFile, SCPluginMetaData} from '@openstapps/core';
import {Validator} from '@openstapps/core-tools/lib/validate';
import config from 'config';
import {BackendTransport} from './notification/backend-transport';

/**
 * Instance of the transport for sending mails
 */
export const mailer = BackendTransport.getTransportInstance();

/**
 * Config file content
 */
export const configFile: SCConfigFile = config.util.toObject();

/**
 * A validator instance to check if something is a valid JSON object (e.g. a request or a thing)
 */
export const validator = new Validator();

/**
 * Provides information if the backend is executed in the "test" (non-production) environment
 */
export const isTestEnvironment = process.env.NODE_ENV !== 'production';

/*
 * Stores a ("key-value") list of plugins where key is route and value is plugin information
 */
export const plugins = new Map<string, SCPluginMetaData>();

/**
 * The version of the installed core
 */
export const coreVersion: string = configFile.backend.SCVersion;

/**
 * The default timeout in milliseconds
 */
export const DEFAULT_TIMEOUT = 20_000;
