/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import express_prom_bundle from 'express-prom-bundle';
import fs from 'fs';
import path from 'path';

/**
 * Create and configure a new Express Prometheus Middleware instance
 *
 * This function tries to configure the new instance with JSON read from
 * `./conf/prometheus.json`. When this fails an instance configured with
 * default options is returned.
 *
 * @returns express.Express
 */
export function getPrometheusMiddleware(): express_prom_bundle.Middleware {
  const configFileName = path.join('./config', 'prometheus.json');
  let options: express_prom_bundle.Opts = {};

  try {
    options = JSON.parse(fs.readFileSync(configFileName, 'utf8'));
  } catch (error) {
    Logger.warn("Couldn't get options from config file for Prometheus Middleware.", error);
  }

  return express_prom_bundle(options);
}
