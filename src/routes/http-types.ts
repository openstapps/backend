/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
// the list provides option to easily implement "isHttpMethod" guard
const httpVerbs = [
  'get',
  'post',
  'put',
  'delete',
  'patch',
  'options',
  'head',
  'checkout',
  'copy',
  'lock',
  'merge',
  'mkactivity',
  'mkcol',
  'move',
  'm-search',
  'notify',
  'purge',
  'report',
  'search',
  'subscribe',
  'trace',
  'unlock',
  'unsubscribe',
] as const;
/**
 * Strings that can be used as HTTP verbs (e.g. in requests): 'get' | 'post' | 'put' | 'delete' etc.
 */
export type HTTPVerb = (typeof httpVerbs)[number];

/**
 * Provides information if a text (representing a method) is an HTTP verb
 *
 * @param method A text (representing a method) to check
 */
export function isHttpMethod(method: string): method is HTTPVerb {
  return (httpVerbs as unknown as string[]).includes(method);
}
