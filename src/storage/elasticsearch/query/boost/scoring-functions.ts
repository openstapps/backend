/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {QueryDslFunctionScoreContainer} from '@elastic/elasticsearch/lib/api/types';
import {SCBackendConfigurationSearchBoostingContext, SCSearchContext} from '@openstapps/core';
import {buildFunctionsForBoostingTypes} from './boost-functions';

/**
 * Builds scoring functions from boosting config
 *
 * @param boostings Backend boosting configuration for contexts and types
 * @param context The context of the app from where the search was initiated
 */
export function buildScoringFunctions(
  boostings: SCBackendConfigurationSearchBoostingContext,
  context: SCSearchContext | undefined,
): QueryDslFunctionScoreContainer[] {
  // default context
  let functions = buildFunctionsForBoostingTypes(boostings['default' as SCSearchContext]);

  if (typeof context !== 'undefined' && context !== 'default') {
    // specific context provided, extend default context with additional boosts
    functions = [...functions, ...buildFunctionsForBoostingTypes(boostings[context])];
  }

  return functions;
}
