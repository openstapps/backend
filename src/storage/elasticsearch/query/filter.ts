/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {QueryDslQueryContainer} from '@elastic/elasticsearch/lib/api/types';
import {SCSearchFilter} from '@openstapps/core';
import {buildBooleanFilter} from './filters/boolean';
import {buildAvailabilityFilter} from './filters/availability';
import {buildDateRangeFilter} from './filters/date-range';
import {buildDistanceFilter} from './filters/distance';
import {buildGeoFilter} from './filters/geo';
import {buildNumericRangeFilter} from './filters/numeric-range';
import {buildValueFilter} from './filters/value';

/**
 * Converts Array of Filters to elasticsearch query-syntax
 *
 * @param filter A search filter for the retrieval of the data
 */
export function buildFilter(filter: SCSearchFilter): QueryDslQueryContainer {
  switch (filter.type) {
    case 'value':
      return buildValueFilter(filter);
    case 'availability':
      return buildAvailabilityFilter(filter);
    case 'distance':
      return buildDistanceFilter(filter);
    case 'boolean':
      return buildBooleanFilter(filter);
    case 'numeric range':
      return buildNumericRangeFilter(filter);
    case 'date range':
      return buildDateRangeFilter(filter);
    case 'geo':
      return buildGeoFilter(filter);
  }
}
