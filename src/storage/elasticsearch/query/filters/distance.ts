/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {QueryDslGeoDistanceQuery} from '@elastic/elasticsearch/lib/api/types';
import {SCSearchDistanceFilter} from '@openstapps/core';
import {QueryDslSpecificQueryContainer} from '../../types/util';

/**
 * Converts a distance filter to elasticsearch syntax
 *
 * @param filter A search filter for the retrieval of the data
 */
export function buildDistanceFilter(
  filter: SCSearchDistanceFilter,
): QueryDslSpecificQueryContainer<'geo_distance'> {
  const geoObject: QueryDslGeoDistanceQuery = {
    distance: `${filter.arguments.distance}m`,
    [`${filter.arguments.field}.point.coordinates`]: {
      lat: filter.arguments.position[1],
      lon: filter.arguments.position[0],
    },
  };

  return {
    geo_distance: geoObject,
  };
}
