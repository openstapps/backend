/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCSearchValueFilter} from '@openstapps/core';
import {QueryDslSpecificQueryContainer} from '../../types/util';

/**
 * Converts a value filter to elasticsearch syntax
 *
 * @param filter A search filter for the retrieval of the data
 */
export function buildValueFilter(
  filter: SCSearchValueFilter,
): QueryDslSpecificQueryContainer<'term'> | QueryDslSpecificQueryContainer<'terms'> {
  return Array.isArray(filter.arguments.value)
    ? {
        terms: {
          [`${filter.arguments.field}.raw`]: filter.arguments.value,
        },
      }
    : {
        term: {
          [`${filter.arguments.field}.raw`]: filter.arguments.value,
        },
      };
}
