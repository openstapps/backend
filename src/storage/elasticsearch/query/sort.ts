/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Sort} from '@elastic/elasticsearch/lib/api/types';
import {SCSearchSort} from '@openstapps/core';
import {buildDistanceSort} from './sort/distance';
import {buildDucetSort} from './sort/ducet';
import {buildGenericSort} from './sort/generic';
import {buildPriceSort} from './sort/price';

/**
 * converts query to
 *
 * @param sorts Sorting rules to apply to the data that is being queried
 * @returns an array of sort queries
 */
export function buildSort(sorts: SCSearchSort[]): Sort {
  return sorts.map(sort => {
    switch (sort.type) {
      case 'generic':
        return buildGenericSort(sort);
      case 'ducet':
        return buildDucetSort(sort);
      case 'distance':
        return buildDistanceSort(sort);
      case 'price':
        return buildPriceSort(sort);
    }
  });
}
