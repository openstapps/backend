/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SortOptions} from '@elastic/elasticsearch/lib/api/types';
import {SCDistanceSort} from '@openstapps/core';

/**
 * Converts a distance sort to elasticsearch syntax
 *
 * @param sort A sorting definition
 */
export function buildDistanceSort(sort: SCDistanceSort): SortOptions {
  return {
    _geo_distance: {
      mode: 'avg',
      order: sort.order,
      unit: 'm',
      [`${sort.arguments.field}.point.coordinates`]: {
        lat: sort.arguments.position[1],
        lon: sort.arguments.position[0],
      },
    },
  };
}
