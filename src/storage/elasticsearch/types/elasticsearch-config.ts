/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A configuration for using the Dis Max Query
 *
 * See https://www.elastic.co/guide/en/elasticsearch/reference/5.5/query-dsl-dis-max-query.html for further
 * explanation of what the parameters mean
 */
export interface ElasticsearchQueryDisMaxConfig {
  /**
   * Relative (to a total number of documents) or absolute number to exclude meaningless matches that frequently appear
   */
  cutoffFrequency: number;

  /**
   * The maximum allowed Levenshtein Edit Distance (or number of edits)
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/5.6/common-options.html#fuzziness
   */
  fuzziness: number | string;

  /**
   * Increase the importance (relevance score) of a field
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/5.6/mapping-boost.html
   */
  matchBoosting: number;

  /**
   * Minimal number (or percentage) of words that should match in a query
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/5.6/query-dsl-minimum-should-match.html
   */
  minMatch: string;

  /**
   * Type of the query - in this case 'dis_max' which is a union of its subqueries
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/5.6/query-dsl-dis-max-query.html
   */
  queryType: 'dis_max';

  /**
   * Changes behavior of default calculation of the score when multiple results match
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/5.6/query-dsl-multi-match-query.html#tie-breaker
   */
  tieBreaker: number;
}

/**
 * A configuration for using Query String Query
 *
 * See https://www.elastic.co/guide/en/elasticsearch/reference/5.5/query-dsl-query-string-query.html for further
 * explanation of what the parameters mean
 */
export interface ElasticsearchQueryQueryStringConfig {
  /**
   * Minimal number (or percentage) of words that should match in a query
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/5.6/query-dsl-minimum-should-match.html
   */
  minMatch: string;

  /**
   * Type of the query - in this case 'query_string' which uses a query parser in order to parse content
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/5.6/query-dsl-query-string-query.html
   */
  queryType: 'query_string';
}

/**
 * An config file for the elasticsearch database interface
 *
 * The config file extends the SCConfig file by further defining how the database property
 */
export interface ElasticsearchConfigFile {
  /**
   * Configuration that is not visible to clients
   */
  internal: {
    /**
     * Database configuration
     */
    database: ElasticsearchConfig;
  };
}

/**
 * An elasticsearch configuration
 */
export interface ElasticsearchConfig {
  /**
   * Name of the database
   */
  name: 'elasticsearch';

  /**
   * Configuration for using queries
   */
  query?: ElasticsearchQueryDisMaxConfig | ElasticsearchQueryQueryStringConfig;

  /**
   * Version of the used elasticsearch
   */
  version: string;
}
