/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

export interface RetryOptions<T> {
  maxRetries: number;
  retryInterval: number;
  doAction: () => Promise<T>;
  onFailedAttempt: (attempt: number, error: unknown, options: RetryOptions<T>) => void;
  onFail: (options: RetryOptions<T>) => never;
}

/**
 * Retries a throwing function at a set interval, until a maximum amount of attempts
 */
export async function retryCatch<T>(options: RetryOptions<T>): Promise<T> {
  for (let attempt = 0; attempt < options.maxRetries; attempt++) {
    try {
      return await options.doAction();
    } catch (error) {
      options.onFailedAttempt(attempt, error, options);
      await new Promise(resolve => setTimeout(resolve, options.retryInterval));
    }
  }

  options.onFail(options);
}
