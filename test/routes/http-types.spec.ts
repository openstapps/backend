/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {SCRouteHttpVerbs} from '@openstapps/core';
import {expect} from 'chai';
import {isHttpMethod} from '../../src/routes/http-types';

describe('Is HTTP method', async function () {
  it('should allow valid (predefined) http methods', async function () {
    // take methods names from SCRouteHttpVerbs
    const validMethods = Object.values(SCRouteHttpVerbs).map(v => v.toLowerCase());

    for (const method of validMethods) {
      expect(isHttpMethod(method)).to.be.true;
    }
  });

  it('should disallow http methods that are not valid (predefined)', async function () {
    const invalidMethods = ['foo', 'bar'];

    for (const method of invalidMethods) {
      expect(isHttpMethod(method)).to.be.false;
    }
  });
});
