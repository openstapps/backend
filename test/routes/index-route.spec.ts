/*
 * Copyright (C) 2019, 2020 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {DEFAULT_TEST_TIMEOUT} from '../common';
import {testApp} from '../tests-setup';
import {SCIndexRequest, SCIndexRoute} from '@openstapps/core';
import {expect} from 'chai';

describe('Index route', async function () {
  // increase timeout for the suite
  this.timeout(DEFAULT_TEST_TIMEOUT);
  const indexRoute = new SCIndexRoute();

  it('should respond with both app and backend configuration', async function () {
    const request: SCIndexRequest = {};

    const response = await testApp
      .post(indexRoute.urlPath)
      .set('Content-Type', 'application/json')
      .send(request);

    expect(response.type).to.equal('application/json');
    expect(response.status).to.equal(indexRoute.statusCodeSuccess);
    expect(response.body).to.haveOwnProperty('app');
    expect(response.body).to.haveOwnProperty('backend');
    expect(response.body).to.haveOwnProperty('auth');
  });
});
