/*
 * Copyright (C) 2019, 2020 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCMethodNotAllowedErrorResponse,
  SCMultiSearchRoute,
  SCSearchRoute,
  SCSyntaxErrorResponse,
  SCTooManyRequestsErrorResponse,
} from '@openstapps/core';
import {expect} from 'chai';
import {configFile} from '../../src/common';
import {DEFAULT_TEST_TIMEOUT} from '../common';
import {testApp} from '../tests-setup';
import sinon from 'sinon';

describe('Search route', async function () {
  // increase timeout for the suite
  this.timeout(DEFAULT_TEST_TIMEOUT);
  const searchRoute = new SCSearchRoute();
  const multiSearchRoute = new SCMultiSearchRoute();
  const syntaxError = new SCSyntaxErrorResponse('Foo Message');
  const methodNotAllowedError = new SCMethodNotAllowedErrorResponse();
  const tooManyRequestsError = new SCTooManyRequestsErrorResponse();

  it('should reject GET, PUT with a valid search query', async function () {
    // const expectedParams = JSON.parse(JSON.stringify(defaultParams));
    const {status} = await testApp.get('/search').set('Accept', 'application/json').send({
      query: 'Some search terms',
    });

    expect(status).to.equal(methodNotAllowedError.statusCode);
  });

  describe('Basic POST /search', async function () {
    it('should accept empty JSON object', async function () {
      const {status} = await testApp.post(searchRoute.urlPath).set('Accept', 'application/json').send({});

      expect(status).to.equal(searchRoute.statusCodeSuccess);
    });

    it('should accept valid search request', async function () {
      const {status} = await testApp.post(searchRoute.urlPath).set('Accept', 'application/json').send({
        query: 'Some search terms',
      });

      expect(status).to.equal(searchRoute.statusCodeSuccess);
    });

    it('should respond with bad request on invalid search request (body)', async function () {
      const {status} = await testApp
        .post(searchRoute.urlPath)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .send({
          some: {invalid: 'search'},
        });

      expect(status).to.equal(syntaxError.statusCode);
    });
  });

  describe('Basic POST /multi/search', async function () {
    it('should respond with bad request on invalid search request (empty JSON object as body)', async function () {
      const {status} = await testApp
        .post(multiSearchRoute.urlPath)
        .set('Accept', 'application/json')
        .send({});

      expect(status).to.equal(multiSearchRoute.statusCodeSuccess);
    });

    it('should accept valid search request', async function () {
      const {status} = await testApp
        .post(multiSearchRoute.urlPath)
        .set('Accept', 'application/json')
        .send({
          one: {query: 'Some search terms for one search'},
          two: {query: 'Some search terms for another search'},
        });

      expect(status).to.equal(multiSearchRoute.statusCodeSuccess);
    });

    it('should respond with too many requests error if the number of sub-queries exceed their max number', async function () {
      const sandbox = sinon.createSandbox();
      sandbox.stub(configFile.backend, 'maxMultiSearchRouteQueries').value(2);

      const {status} = await testApp
        .post(multiSearchRoute.urlPath)
        .set('Content-Type', 'application/json')
        .send({
          one: {},
          two: {},
          three: {},
        });

      expect(status).to.equal(tooManyRequestsError.statusCode);
      sandbox.restore();
    });
  });
});
