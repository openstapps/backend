/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingUpdateRoute} from '@openstapps/core';
import chaiAsPromised from 'chai-as-promised';
import {bulkStorageMock, DEFAULT_TEST_TIMEOUT} from '../common';
import {expect, use} from 'chai';
import {instance as book} from '@openstapps/core/test/resources/indexable/Book.1.json';
import {testApp} from '../tests-setup';

use(chaiAsPromised);

describe('Thing update route', async function () {
  // increase timeout for the suite
  this.timeout(DEFAULT_TEST_TIMEOUT);
  const thingUpdateRoute = new SCThingUpdateRoute();

  it('should update a thing', async function () {
    const thingUpdateRouteurlPath = thingUpdateRoute.urlPath
      .toLocaleLowerCase()
      .replace(':type', book.type)
      .replace(':uid', book.uid);

    const {status} = await testApp
      .put(thingUpdateRouteurlPath)
      .set('Content-Type', 'application/json')
      .send(book);

    expect(status).to.equal(thingUpdateRoute.statusCodeSuccess);
    expect(bulkStorageMock.database.get(book.uid)).to.eventually.be.deep.equal(book);
  });
});
