/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {AggregateName, AggregationsMultiTermsBucket} from '@elastic/elasticsearch/lib/api/types';
import {SCFacet, SCThingType} from '@openstapps/core';
import {expect} from 'chai';
import {parseAggregations} from '../../../src/storage/elasticsearch/aggregations';

describe('Aggregations', function () {
  const aggregations: Record<AggregateName, Partial<AggregationsMultiTermsBucket>> = {
    'catalog': {
      'doc_count': 4,
      'superCatalogs.categories': {
        buckets: [],
      },
      'academicTerm.acronym': {
        buckets: [
          {
            key: 'SoSe 2020',
            doc_count: 2,
          },
        ],
      },
      'superCatalog.categories': {
        buckets: [],
      },
      'categories': {
        buckets: [
          {
            key: 'foo',
            doc_count: 1,
          },
          {
            key: 'bar',
            doc_count: 3,
          },
        ],
      },
    },
    'person': {
      'doc_count': 13,
      'homeLocations.categories': {
        buckets: [],
      },
    },
    'academic event': {
      'doc_count': 0,
      'academicTerms.acronym': {
        buckets: [],
      },
      'categories': {
        buckets: [
          {
            key: 'foobar',
            doc_count: 8,
          },
          {
            key: 'bar',
            doc_count: 2,
          },
        ],
      },
      'creativeWorks.keywords': {
        buckets: [],
      },
    },
    '@all': {
      doc_count: 17,
      type: {
        buckets: [
          {
            key: 'person',
            doc_count: 13,
          },
          {
            key: 'catalog',
            doc_count: 4,
          },
        ],
      },
    },
  };

  const expectedFacets: SCFacet[] = [
    {
      buckets: [
        {
          count: 2,
          key: 'SoSe 2020',
        },
      ],
      field: 'academicTerm.acronym',
      onlyOnType: SCThingType.Catalog,
    },
    {
      buckets: [
        {
          count: 1,
          key: 'foo',
        },
        {
          count: 3,
          key: 'bar',
        },
      ],
      field: 'categories',
      onlyOnType: SCThingType.Catalog,
    },
    {
      buckets: [
        {
          count: 8,
          key: 'foobar',
        },
        {
          count: 2,
          key: 'bar',
        },
      ],
      field: 'categories',
      onlyOnType: SCThingType.AcademicEvent,
    },
    {
      buckets: [
        {
          count: 13,
          key: 'person',
        },
        {
          count: 4,
          key: 'catalog',
        },
      ],
      field: 'type',
    },
  ];

  it('should parse the aggregations providing the appropriate facets', function () {
    const facets = parseAggregations(aggregations);

    expect(facets).to.be.eql(expectedFacets);
  });
});
